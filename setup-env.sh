#!/bin/bash
#
# Setup my shell environment

_script_dir=${0%/*}
if [[ `whoami` = 'root' ]]; then sudo=''; else sudo='sudo'; fi
export sudo

_echo() {
  echo -e $1
}
export -f _echo

_ask_yes() {
  yes=''
  read -p "$1 (y/n) " yes
  if [[ $yes = 'y' ]]; then
    return 0
  elif [[ $yes = 'n' ]]; then
    return 1
  else
    _ask_yes
  fi
}
export -f _ask_yes

_check_app() {
  if [[ `which $1` ]]; then return; fi
  _echo "$1 not found"
  return 1
}
export -f _check_app

_init_setup() {
  _echo 'Initializing setup script...'
  cwd=`mktemp -d`
  cp -lr $_script_dir/* $cwd/
  cd $cwd
  $sudo apt update >> /dev/null 2>&1
  $sudo apt install -y curl unzip >> /dev/null 2>&1
  _echo 'Done!\n'
}

_run_all_setup_shell() {
  for setup in `ls -1 $_script_dir/setup-*.sh`
  do
    if [[ $setup = $0 ]]; then continue; fi
    echo "Run setup script: $setup"
    chmod u+x $setup
    $setup
  done
}

_init_setup || exit
_run_all_setup_shell

