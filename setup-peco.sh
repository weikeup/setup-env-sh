#!/bin/bash
# Setup peco

_check_app peco && exit
_ask_yes 'Install?' || exit $?
_echo 'Installing...'
$sudo apt install -y peco >> /dev/null 2>&1
_check_app peco && _echo 'Install success!\n'
