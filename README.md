# setup-env-sh

This shell scipt will set up my environment.

## Installed Apps

- [exa](https://github.com/ogham/exa)
- [peco](https://github.com/peco/peco)
- [fish-shell](https://github.com/fish-shell/fish-shell)
- [fisher](https://github.com/jorgebucaran/fisher)
- [z](https://github.com/jethrokuan/z)
- [Tide](https://github.com/IlanCosman/tide)
