#!/bin/bash
# Setup nvim

_check_app nvim && exit
_ask_yes 'Install?' || exit $?
_echo 'Installing...'

# Download nvim.appimage to /usr/bin/nvim
$sudo curl -sfLo /usr/bin/nvim https://github.com/neovim/neovim/releases/download/stable/nvim.appimage
$sudo chmod a+x /usr/bin/nvim

# Install FUSE library to run app image
$sudo apt install libfuse2 -y >> /dev/null 2>&1

_check_app nvim && _echo 'Install success!\n'
