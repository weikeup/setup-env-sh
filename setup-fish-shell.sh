#!/bin/bash
# Setup fish-shell

_setup_fish() {
  _ask_yes 'Install?' || exit $?
  _echo 'Installing...'
  # Add fish-shell repository
  $sudo apt-add-repository -y ppa:fish-shell/release-3 >> /dev/null

  $sudo apt update >> /dev/null 2>&1
  $sudo apt install -y fish  >> /dev/null 2>&1
  chsh -s /usr/bin/fish
  _check_app fish && _echo 'Install success!\n' || exit $?
}

_setup_plugins() {
  _echo 'Installing fish plugins...'
  # Setup fisher
  curl -sL https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish | fish -c 'source && fisher install jorgebucaran/fisher'

  fish -c 'fisher install IlanCosman/tide@v5'
  fish -c 'fisher install jorgebucaran/nvm.fish'
  fish -c 'fisher install jethrokuan/z'
  fish -c 'fisher install PatrickF1/fzf.fish'
  fish -c 'fisher install wfxr/forgit'
}

_check_app fish || _setup_fish
_check_app fish && _setup_plugins
